
Program for corrupt files. Used for testing the Hamming(7,4)

Compile with g++ in standard c++11

Arguments recieved
  arg1: The input file to corrupt
  arg2: The output name file (same extension as input)
  arg3: (Optional) Probability of change a byte, using a bernoulli distribuition

The program prints the byte and bite changed, so in the exec use the '>' for export
in a log file, example

  .\my_corrupt.exe file.hex corrupt.hex 0.2 > log_change.txt
  
