#include <random>
#include <iostream>
#include <fstream>
#include <ctime>
#include <string>
#include <chrono>


unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();
std::default_random_engine generator(seed1);

int main(int argc, char const *argv[]) {

  if(argc != 4 && argc != 3){
    std::cout << "Arguments: \n \t arg1 -> File to corrupt \n \t arg2 -> Output name (same extension as input) \n \t arg3 -> Probability of change a byte (optional) \n";
    return 0;
  }

  float p = 0.5;
  if(argc == 4){
    float f = std::stof(std::string(argv[3]));
    if(f > 0 && f < 1){
      p = f;
    }
  }

  std::bernoulli_distribution bernoulli(p);
  std::uniform_int_distribution<int> uniform(0, 7);

  std::ifstream ifs;
  std::ofstream ofs;
  ifs.open(argv[1], std::ios::binary);
  ofs.open(argv[2], std::ios::binary);
  if(!ifs.is_open() || !ofs.is_open()) return 0;
  char c;
  int bit_toogle;
  long count = 0;
  while(ifs.get(c)){
    if(bernoulli(generator)){
      bit_toogle = uniform(generator);
      c ^= 0x01 << bit_toogle;
      ofs << c;
      std::cout << "Se introdujo un error en el byte " << count << ": bit " << bit_toogle << std:: endl;
    }
    else{
      ofs << c;
    }
    count++;
  }

  ifs.close();
  ofs.close();

  return 0;
}
